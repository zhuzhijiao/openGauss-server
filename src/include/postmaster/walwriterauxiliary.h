/* -------------------------------------------------------------------------
 *
 * walwriterauxiliary.h
 *	  Exports from postmaster/walwriterauxiliary.c.
 *
 * src/include/postmaster/walwriterauxiliary.h
 *
 * -------------------------------------------------------------------------
 */
#ifndef _WALWRITERAUXILIARY_H
#define _WALWRITERAUXILIARY_H

extern void WalWriterAuxiliaryMain(void);

#endif   /* _WALWRITERAUXILIARY_H */